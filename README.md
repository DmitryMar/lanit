# Lanit



## Проект стажировки по направлению QA-инженер

Каждая ветка отдельное задание.
- [main](https://gitlab.com/DmitryMar/lanit/-/tree/main) - основная ветка
- [homework1](https://gitlab.com/DmitryMar/lanit/-/tree/homework1) - Java Syntax (создание котика)
- [homework2](https://gitlab.com/DmitryMar/lanit/-/tree/homework2) - Java ООП (создание зоопарка)
- [homework3](https://gitlab.com/DmitryMar/lanit/-/tree/homework3) - Java обобщения, коллекции, перечисления, исключения (улучшение зоопарка)
- homework4 - Java регулярные выражения (Merge requests в ветку main с последующим удалением)
- [unit-master](https://gitlab.com/DmitryMar/lanit/-/tree/unit-master) - Unit  тесты (тестирование калькулятора)
- [unit-develop](https://gitlab.com/DmitryMar/lanit/-/tree/unit-develop) - Unit  тесты (тестирование калькулятора)
- [api-master](https://gitlab.com/DmitryMar/lanit/-/tree/api-master) - тестирование API (Helpdesk API)
- [ui-master](https://gitlab.com/DmitryMar/lanit/-/tree/ui-master) - тестирование UI (Helpdesk UI) (WebDriver + Selenium)
- [allure](https://gitlab.com/DmitryMar/lanit/-/tree/allure) - отчётность Allure
- [cucumer](https://gitlab.com/DmitryMar/lanit/-/tree/cucumer) - [BDD фреймворк](https://github.com/lanit-exp/Quick_Start_BDD)


## homework4
- Задания (базовое) - [regexp-basic.md](src/main/resources/regexp-basic.md)
- Задания повышенной сложности - [regexp-additional.md](src/main/resources/regexp-additional.md)
- Решения базовых заданий - [regexp-basic.properties](src/main/resources/regexp-basic.properties)
- Решение задания повышенной сложности - [regexp-additional.properties](src/main/resources/regexp-additional.properties)