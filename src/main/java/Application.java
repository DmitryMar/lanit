import animals.Kotik;

public class Application {
//    public static void main(String[] args) {
//        Kotik cat1 = new Kotik("Bars","M-m-r-r",9,3);
//        Kotik cat2 = new Kotik();
//        cat2.setName("Kotan");
//        cat2.setVoice("What?");
//        cat2.setSatiety(5);
//        cat2.setWeight(4);
//        String[] day = cat1.liveAnotherDay();
//        for (String s:day) {
//            System.out.println(s);
//        }
//        System.out.println(cat2.getName() + " " + cat2.getWeight());
//        System.out.println(compareVoice(cat2,cat1));
//        System.out.println(Kotik.getCount());
//    }

    public static boolean compareVoice(Kotik c1, Kotik c2){
        if(c1 == null || c2 == null){
            return false;
        }
        return c1.getVoice().equals(c2.getVoice());
    }
}
