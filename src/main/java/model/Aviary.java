package model;

import animals.Animal;

import java.util.HashMap;

public class Aviary <T extends Animal>{
    private Size size;
    private HashMap<String,T> aviaryMap = new HashMap<>();

    public Aviary(Size size) {
        this.size = size;
    }

    public void addAnimal(T t) {
        if(this.size.equals(t.getSize())){
            aviaryMap.put(t.getName(),t);
        } else {
            throw new WrongSizeException();
        }

    }

    public T getAnimal(String s) {
        return (T) aviaryMap.get(s);
    }

    public boolean removeAnimal(String s) {
        if(aviaryMap.containsKey(s)) {
            aviaryMap.remove(s);
            return true;
        }else {
            return false;
        }
    }
}
