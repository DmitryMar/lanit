import animals.*;
import employee.Worker;
import food.Food;
import food.Grass;
import food.Meat;
import model.Aviary;
import model.Size;

import java.util.ArrayList;

public class Zoo {
    private static ArrayList<Animal> zoopark = new ArrayList<>();
//    private static ArrayList<Food> foods = new ArrayList();
//    private static ArrayList<Worker> staff = new ArrayList();
    private static Aviary<Herbivore> herbivoreAviary = new Aviary<>(Size.LARGE);
    private static Aviary<Carnivorous> carnivorousAviary = new Aviary<>(Size.MEDIUM);

    public static void main(String[] args) {

        Meat meat = new Meat();
        Grass grass = new Grass();

        Worker man = new Worker();

        Kotik cat = new Kotik("Kat");

        man.feed(cat,meat);

//        for (Swim s:createPond()) {
//            s.swim();
//        }
    }

    public static Swim[] createPond() {
        int size = 0;
        for (Animal animal: zoopark) {
            if (animal instanceof Swim) {
                ++size;
            }
        }
        Swim[] a = new Swim[size];
        int i = 0;
        for (Animal animal: zoopark) {
            if (animal instanceof Swim) {
                a[i] = (Swim) animal;
                ++i;
            }
        }
        return a;
    }

    public static void fillCarnivorousAviary(){
        Kotik bars = new Kotik("Barsik");
        Kotik puma = new Kotik("Murzic");
        Wolf wolf = new Wolf("Gava");
        Fish fish = new Fish("Nemo");
        Clam clam = new Clam("Luck");

        carnivorousAviary.addAnimal(bars);
        carnivorousAviary.addAnimal(puma);
        carnivorousAviary.addAnimal(wolf);
        carnivorousAviary.addAnimal(fish);
    }

    public static void fillHerbivoreAviary(){
        Clam clam = new Clam("Luck");
        Duck clam1 = new Duck("Duck");
        Duck duck = new Duck("Kryk");
        Rabbit rabbit = new Rabbit("Banny");

        herbivoreAviary.addAnimal(clam);
        herbivoreAviary.addAnimal(clam1);
        herbivoreAviary.addAnimal(duck);
        herbivoreAviary.addAnimal(rabbit);
    }

    public static Carnivorous getCarnivorous(String name){
        return (Carnivorous) carnivorousAviary.getAnimal(name);
    }

    public static Herbivore getHerbivore(String name){
        return (Herbivore)herbivoreAviary.getAnimal(name);
    }
}

