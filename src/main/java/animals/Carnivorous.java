package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name) {
        super(name);
    }

    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            this.setSatiety(food.getEnergy() + this.getSatiety());
            System.out.println("����");
        } else {
            throw new WrongFoodException();
        }

    }
}
