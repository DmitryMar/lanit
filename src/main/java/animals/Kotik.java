package animals;

import model.Size;

public class Kotik extends Carnivorous implements Run, Voice {
    public Kotik(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }

    public void run() {
        System.out.println("����� ������");
    }

    public String getVoice() {
        return "���";
    }
}
