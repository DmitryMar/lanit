package animals;

import model.Size;

public class Clam extends Herbivore {
    public Clam(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }
}