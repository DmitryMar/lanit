package animals;

import model.Size;

public class Wolf extends Carnivorous implements Voice, Run, Swim {
    public Wolf(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.MEDIUM;
    }

    public void run() {
    }

    public void swim() {
    }

    public String getVoice() {
        return "�-�-�-�-�-�";
    }
}
