package animals;

import food.Food;
import food.WrongFoodException;
import model.Size;

public abstract class Animal {
    private int satiety;
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSatiety() {
        return this.satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public abstract void eat(Food var1) throws WrongFoodException;

    public abstract Size getSize();
}