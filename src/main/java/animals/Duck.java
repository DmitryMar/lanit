package animals;

import model.Size;

public class Duck extends Herbivore implements Fly, Run, Swim, Voice {
    public Duck(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }

    public void fly() {
        System.out.println("������ ������");
    }

    public void run() {
        System.out.println("������ ����� �� ������");
    }

    public void swim() {
        System.out.println("������ ����� �� ����");
    }

    public String getVoice() {
        return "���-���";
    }
}
