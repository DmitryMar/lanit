package animals;

import model.Size;

public class Rabbit extends Herbivore implements Run, Voice {
    public Rabbit(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }

    public void run() {
        System.out.println("������ ������");
    }

    public String getVoice() {
        return "�-� �-�";
    }
}