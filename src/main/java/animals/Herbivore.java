package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;

public abstract class Herbivore extends Animal {
    public Herbivore(String name) {
        super(name);
    }

    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            this.setSatiety(food.getEnergy() + this.getSatiety());
            System.out.println("����");
        } else {
            throw new WrongFoodException();
        }

    }
}
