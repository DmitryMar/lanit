package food;

public abstract class Food {
    public Food() {
    }

    public abstract int getEnergy();
}
